package com.example.order_food.model;

public class FoodDaily {
    String idFood;

    public FoodDaily() {
    }

    public FoodDaily(String idFood) {
        this.idFood = idFood;
    }

    public String getIdFood() {
        return idFood;
    }

    public void setIdFood(String idFood) {
        this.idFood = idFood;
    }
}
