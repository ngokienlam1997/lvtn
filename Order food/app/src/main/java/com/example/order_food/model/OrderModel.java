package com.example.order_food.model;

public class OrderModel {
    String name;
    String imageURL;
    int Price;

    public OrderModel(String name, String imageURL, int price) {
        this.name = name;
        this.imageURL = imageURL;
        Price = price;
    }

    public OrderModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }
}
