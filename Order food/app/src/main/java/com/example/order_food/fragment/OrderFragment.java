package com.example.order_food.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.order_food.Adapter.OrderAdapter;
import com.example.order_food.R;
import com.example.order_food.model.OrderModel;

import java.util.ArrayList;

public class OrderFragment extends Fragment {

    ////XML
    View view;
    RecyclerView recyclerView;
    TextView txt_order;
    Button btn_deli;
    ArrayList<OrderModel> orderModels;
    OrderAdapter orderAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order, container, false);

        ////Mapping
        recyclerView = view.findViewById(R.id.rc_order);
        txt_order = view.findViewById(R.id.txt_order);
        btn_deli = view.findViewById(R.id.btn_deli);

        ////RecycleView
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        String[] icon = {String.valueOf(R.drawable.ic_banhmi), String.valueOf(R.drawable.ic_milktea)};
        String[] name = {"bánh mì", "trà sửa"};
        Integer[] price = {40000, 45000};

        orderModels = new ArrayList<>();
        for (int i = 0; i < icon.length; i++) {
            OrderModel model = new OrderModel(name[i] , icon[i], price[i]);
            orderModels.add(model);
        }

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        orderAdapter = new OrderAdapter(orderModels, getContext());
        recyclerView.setAdapter(orderAdapter);

        return view;
    }
}