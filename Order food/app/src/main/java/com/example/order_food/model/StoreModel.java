package com.example.order_food.model;

public class StoreModel {
    String name, address, image, phone, status;

    public StoreModel(String name, String address, String image, String phone, String status) {
        this.name = name;
        this.address = address;
        this.image = image;
        this.phone = phone;
        this.status = status;
    }

    public StoreModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
