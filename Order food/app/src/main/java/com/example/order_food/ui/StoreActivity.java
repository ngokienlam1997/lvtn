package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.order_food.Adapter.StoreAdapter;
import com.example.order_food.R;
import com.example.order_food.model.FoodModel;
import com.example.order_food.model.StoreModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StoreActivity extends AppCompatActivity {

    ////XML
    Toolbar toolbar;
    RecyclerView view;
    StoreAdapter storeAdapter;
    List<StoreModel> mStores = new ArrayList<>();
    FirebaseFirestore mDatabase;
    ProgressDialog progressDialog;
    String idStore;
    List<FoodModel> mFoods = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        mDatabase = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Vui long cho ...");
        progressDialog.setCanceledOnTouchOutside(false);
        ////Mapping
        toolbar = findViewById(R.id.tb_store);
        view = findViewById(R.id.recycler_storedetail);

        //set Transparent and Toolbar
        toolbar.setPadding(0, getStatusHeight(), 0, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Food");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.item_add) {
                    startActivity(new Intent(StoreActivity.this, AddStoreActivity.class));
                }
                return false;
            }
        });

        storeAdapter = new StoreAdapter(this, mStores);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        view.setLayoutManager(mLayoutManager);
        view.setItemAnimator(new DefaultItemAnimator());
        view.setAdapter(storeAdapter);
        storeAdapter.setOnItemClickListener((itemView, position)
                -> {
            Intent intent = new Intent(StoreActivity.this, FoodActivity.class);
            intent.putExtra("KEY_ID", mStores.get(position).getAddress());
            startActivity(intent);
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataListStore();
    }

    public void getDataListStore(){
        progressDialog.show();
        mDatabase.collection("store").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<StoreModel> mStore = new ArrayList<>();
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        idStore = document.getId();
                        StoreModel store = document.toObject(StoreModel.class);
                        mStore.add(store);
                    }
                    mStores.clear();
                    mStores.addAll(mStore);
                    storeAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(StoreActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    //
                }
            }
        });
    }

    ////Transparent
    private int getStatusHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return true;
    }
}
