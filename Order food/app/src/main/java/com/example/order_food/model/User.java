package com.example.order_food.model;

public class User {
    private String id, address, fristName, lastName, phone, imageURL, role, nameStore;

    public User() {
    }

    public User(String id, String address, String fristName, String lastName, String phone, String imageURL, String role, String nameStore) {
        this.id = id;
        this.address = address;
        this.fristName = fristName;
        this.lastName = lastName;
        this.phone = phone;
        this.role = role;
        this.imageURL = imageURL;
        this.nameStore = nameStore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFristName() {
        return fristName;
    }

    public void setFristName(String fristName) {
        this.fristName = fristName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNameStore() {
        return nameStore;
    }

    public void setNameStore(String nameStore) {
        this.nameStore = nameStore;
    }
}
