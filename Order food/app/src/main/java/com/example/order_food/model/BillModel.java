package com.example.order_food.model;

public class BillModel {
    String address, cast, iddetail, image, name, status, timeday;

    public BillModel() {
    }

    public BillModel(String address, String cast, String iddetail, String image, String name, String status, String timeday) {
        this.address = address;
        this.cast = cast;
        this.iddetail = iddetail;
        this.image = image;
        this.name = name;
        this.status = status;
        this.timeday = timeday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getIddetail() {
        return iddetail;
    }

    public void setIddetail(String iddetail) {
        this.iddetail = iddetail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeday() {
        return timeday;
    }

    public void setTimeday(String timeday) {
        this.timeday = timeday;
    }
}
