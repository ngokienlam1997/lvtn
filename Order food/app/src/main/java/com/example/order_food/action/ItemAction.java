package com.example.order_food.action;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.example.order_food.ui.AddFoodActivity;
import com.example.order_food.ui.AddStoreActivity;

public abstract class ItemAction implements OnItemListener {

    Context context;
    private OnItemListener onItemListener;

    @Override
    public void onTap(View view, int position) {

    }

    @Override
    public void onLongTap(int position) {
    }

}
