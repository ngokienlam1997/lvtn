package com.example.order_food.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.order_food.R;
import com.example.order_food.action.OnItemListener;
import com.example.order_food.model.StoreModel;
import com.example.order_food.ui.AddFoodActivity;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.List;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<StoreModel> mStore;

    private static OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public StoreAdapter( Context context, List<StoreModel> models) {
        this.context = context;
        this.mStore = models;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        StoreModel mStores = mStore.get(position);
        holder.name.setText(mStores.getName());
        holder.address.setText(mStores.getAddress());
        holder.phone.setText(mStores.getPhone());
        if (mStores.getImage() != null) {
            byte[] decodedString = Base64.decode(mStores.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.imageView.setImageBitmap(decodedByte);
        } else {
            holder.imageView.setImageBitmap(null);
        }
    }

    @Override
    public int getItemCount() {
        return mStore.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.store_adapter, parent, false);
        return new MyViewHolder(item);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, address, phone;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_store_name_adapter);
            address = itemView.findViewById(R.id.txt_store_address_adapter);
            phone = itemView.findViewById(R.id.txt_store_phone_adapter);
            imageView = itemView.findViewById(R.id.img_store_adapter);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
