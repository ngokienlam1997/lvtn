package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.order_food.Adapter.AddOrderAdapter;
import com.example.order_food.Adapter.FoodAdapter;
import com.example.order_food.R;
import com.example.order_food.model.FoodModel;
import com.example.order_food.model.StoreModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddOderActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView view;
    FirebaseFirestore mDatabase;
    ImageView img_addstore;
    TextView txt_addstore;
    String addressStore, idFood, idStore;
    ProgressDialog progressDialog;
    AddOrderAdapter adapter;
    List<FoodModel> mFoods = new ArrayList<>();
    List<StoreModel> mStores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_oder);

        toolbar = findViewById(R.id.tb_addorder);
        view = findViewById(R.id.view_addorder);
        img_addstore = findViewById(R.id.img_addstore);
        txt_addstore = findViewById(R.id.txt_addstore);

        toolbar();
        mDatabase = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Vui long cho ...");
        progressDialog.setCanceledOnTouchOutside(false);

        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            addressStore = (String) b.get("KEY_NAME");
        }

        adapter = new AddOrderAdapter(this, mFoods);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        view.setLayoutManager(mLayoutManager);
        view.setItemAnimator(new DefaultItemAnimator());
        view.setAdapter(adapter);

    }

    ////Transparent
    private int getStatusHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }

    private void toolbar() {
        toolbar.setPadding(0, getStatusHeight(), 0, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataStore();
        getDataListStore();
    }

    public void getDataListStore(){
        progressDialog.show();
        mDatabase.collection("food").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<FoodModel> mFood = new ArrayList<>();
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        idFood = document.getId();
                        FoodModel food = document.toObject(FoodModel.class);
                        mFood.add(food);
                    }
                    mFoods.clear();
                    for (int i = 0; i < mFood.size(); i++){
                        if (mFood.get(i).getIdstore().equals(addressStore)){
                            mFoods.add(mFood.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddOderActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    //
                }
            }
        });
    }

    public void getDataStore(){
        progressDialog.show();
        mDatabase.collection("store").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    List<StoreModel> mStore = new ArrayList<>();
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())){
                        idStore = document.getId();
                        StoreModel model = document.toObject(StoreModel.class);
                        mStore.add(model);
                    }
                    mStores.clear();
                    for (int i = 0; i < mStore.size(); i++){
                        if (mStore.get(i).getAddress().equals(addressStore)){
                            if (mStore.get(i).getImage() != null){
                                byte[] decodedString = Base64.decode(mStore.get(i).getImage(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                img_addstore.setImageBitmap(decodedByte);
                            } else {
                                img_addstore.setImageBitmap(null);
                            }
                            txt_addstore.setText(mStore.get(i).getName());
                            mStores.add(mStore.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddOderActivity.this, "Load không thành công", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}