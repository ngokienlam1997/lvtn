package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.order_food.R;
import com.example.order_food.fragment.HomeFragment;
import com.example.order_food.fragment.MapFragment;
import com.example.order_food.fragment.OrderFragment;
import com.example.order_food.fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private Toolbar toolbar;
    ImageSlider imageSlider;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        frameLayout = findViewById(R.id.fl_main);
        BottomNavigationView navigationView = findViewById(R.id.bot_navi_menu);
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);


        //set Transparent
        toolbar.setPadding(0, getStatusHeight(), 0, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //set Home selected
        navigationView.setSelectedItemId(R.id.botNaviHome);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()){
                case R.id.botNaviHome:
                    fragment = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fl_main, fragment).commit();
                    toolbar.setTitle(R.string.navi_home);
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.botNaviAssign:
                    toolbar.setTitle(R.string.navi_order);
                    fragment = new OrderFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.botNaviMap:
                    toolbar.setTitle(R.string.navi_map);
                    fragment = new MapFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.botNaviAccount:
                    SharedPreferences.Editor editor = getSharedPreferences("PREFS",MODE_PRIVATE).edit();
                    editor.putString("profileid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                    editor.apply();
                    toolbar.setTitle(R.string.navi_profile);
                    fragment = new ProfileFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    ////Fragment
    private void loadFragment(Fragment fragment) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fl_main, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
    }
    ////Fragment

    ////Transparent
    private int getStatusHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }
    ////Transparent
}