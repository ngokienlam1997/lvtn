package com.example.order_food.action;

import android.view.View;

public interface OnItemListener {
    void onTap(View view, int position);
    void onLongTap(int position);
}
