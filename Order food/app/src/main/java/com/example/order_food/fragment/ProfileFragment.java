package com.example.order_food.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.order_food.R;
import com.example.order_food.model.User;
import com.example.order_food.ui.EditProfileActivity;
import com.example.order_food.ui.AddFoodActivity;
import com.example.order_food.ui.StoreActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    ////XML
    View view;
    CircleImageView circleImageView;
    TextView txtRole, txtName, txtAddress, txtPhone;
    LinearLayout edtProfile, edtStore;
    ////XML

    ////Firebase
    FirebaseUser firebaseUser;
    String profileID;
    User user = new User();
    ProgressDialog progressDialog;
    ////Firebase

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ////Mapping XML
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        circleImageView = view.findViewById(R.id.profile_image);
        txtName = view.findViewById(R.id.txt_name);
        txtAddress = view.findViewById(R.id.txt_address);
        txtPhone = view.findViewById(R.id.txt_phone);
        txtRole = view.findViewById(R.id.txt_role);
        edtProfile = view.findViewById(R.id.edt_profile);
        edtStore = view.findViewById(R.id.edt_store);

        ////Dialog
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Waiting...");
        progressDialog.setMessage("Đang lấy thông tin ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        ////Firebase
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences prefs = getContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        profileID = prefs.getString("profileid", "none");

        profileUser();

        edtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
        edtStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), StoreActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    private void profileUser() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child("GDVgWsiC6DPBlg9OfM5pWGlXRqU2");
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (getContext() == null) {
                    return;
                }

                user = snapshot.getValue(User.class);
                if(user != null) {
                    Glide.with(getContext()).load(user.getImageURL()).into(circleImageView);
                    txtName.setText(user.getFristName() + user.getLastName());
                    txtAddress.setText(user.getAddress());
                    txtPhone.setText(user.getPhone());
                    setRole(Integer.parseInt(user.getRole()));
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void setRole(int value) {
        switch (value) {
            case 0:
                txtRole.setText("Cửa hàng");
                break;
            case 1:
                txtRole.setText("Tài xế");
                break;
            default:
                txtRole.setText("Khách hàng");
                break;
        }
    }

}