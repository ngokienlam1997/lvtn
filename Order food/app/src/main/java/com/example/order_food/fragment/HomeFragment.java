package com.example.order_food.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.order_food.Adapter.ListStoreAdapter;
import com.example.order_food.Adapter.CategoryAdapter;
import com.example.order_food.R;
import com.example.order_food.model.IconModel;
import com.example.order_food.model.StoreModel;
import com.example.order_food.service.AppLocationService;
import com.example.order_food.service.LocationAddress;
import com.example.order_food.ui.AddOderActivity;
import com.example.order_food.ui.ListFoodActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class HomeFragment extends Fragment implements LocationListener {

    ////XML
    View view;
    RecyclerView viewCatelogy, viewStore;
    TextView txt_location, txt_monan, txt_free;
    CategoryAdapter adapterCategory;
    ListStoreAdapter adapterStore;
    List<SlideModel> slideModels;
    ImageSlider imageSlider, slider;

    List<IconModel> mCate = new ArrayList<>();
    List<StoreModel> mStore = new ArrayList<>();
    FirebaseFirestore mDatabase;
    String idCategory, idStore;

    ////Location
    private FusedLocationProviderClient fusedLocationProviderClient;
    LocationManager locationManager;
    AppLocationService appLocationService;

    ////Dialog
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        ////Mapping
        viewCatelogy = view.findViewById(R.id.recycler_home);
        viewStore = view.findViewById(R.id.recycler_homemonan);
        txt_location = view.findViewById(R.id.txt_location);
        txt_monan = view.findViewById(R.id.txt_xemmon);

        mDatabase = FirebaseFirestore.getInstance();


        ////RecycleView Catagory
        ////Design LayoutManager
        adapterCategory = new CategoryAdapter(mCate, getContext());
        RecyclerView.LayoutManager mLayoutManagerCate = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        viewCatelogy.setLayoutManager(mLayoutManagerCate);
        viewCatelogy.setItemAnimator(new DefaultItemAnimator());
        viewCatelogy.setAdapter(adapterCategory);
        adapterCategory.setOnItemClickListener((itemView, position)
                -> {
            Intent intent = new Intent(getContext(), ListFoodActivity.class);
            intent.putExtra("KEY_CATE", mCate.get(position).getName());
            startActivity(intent);
        });

        adapterStore = new ListStoreAdapter(mStore, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        viewStore.setLayoutManager(mLayoutManager);
        viewStore.setItemAnimator(new DefaultItemAnimator());
        viewStore.setAdapter(adapterStore);
        adapterStore.setOnItemClickListener((itemView, position)
                -> {
            Intent intent = new Intent(getContext(), AddOderActivity.class);
            intent.putExtra("KEY_NAME", mStore.get(position).getAddress());
            startActivity(intent);
        });

        ////Dialog
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Vui long cho ...");
        progressDialog.setCanceledOnTouchOutside(false);


        //Banner
        slideModels = new ArrayList<>();
        slideModels.add(new SlideModel("https://cdn.shopify.com/s/files/1/0005/5335/3267/files/ff_large.jpg?v=1569996838"));
        slideModels.add(new SlideModel("https://cdn.mylittleadventure.com/articles/india_gastronomy_banner.jpg"));
        slideModels.add(new SlideModel("https://t3.ftcdn.net/jpg/02/96/54/26/240_F_296542619_mj6Gw2yQzUNbpBXDsruZvwCWWzVJD9hp.jpg"));
        imageSlider = view.findViewById(R.id.banner_image_slider);
        slider = view.findViewById(R.id.banner_imageslider);
        imageSlider.setImageList(slideModels, true);
        slider.setImageList(slideModels, true);

        ////Location
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getActivity()));
        onRequestPermissions();
        onGetLocation();

        ////Set TextView
        getTextView();
        getDataCategory();
        getDataFoodDaily();

        return view;
    }

    ////Location
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            txt_location.setText(locationAddress);
        }
    }

    private void onGetLocation() {
        locationManager = (LocationManager) Objects.requireNonNull(getContext()).getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Location location = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(lat, lng, getContext(), new GeocoderHandler());
            }
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,this);
        progressDialog.dismiss();
    }

    private void onRequestPermissions() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions((Activity) getContext(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                ActivityCompat.requestPermissions((Activity) getContext(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return ;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
        }
    }

    ////Set text view
    private void getTextView() {
        String s = "Xem thêm";
        SpannableString spannableString = new SpannableString(s);
        spannableString.setSpan(new UnderlineSpan(), 0,8,0);
        txt_monan.setText(spannableString);
    }

    public void getDataFoodDaily(){
        progressDialog.show();
        mDatabase.collection("store").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    List<StoreModel> mStores = new ArrayList<>();
                    for (DocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        idStore = document.getId();
                        StoreModel store = document.toObject(StoreModel.class);
                        mStores.add(store);
                    }
                    mStore.clear();
                    mStore.addAll(mStores);
                    adapterStore.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(appLocationService, "Không load được dữ liệu", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getDataCategory(){
        progressDialog.show();
        mDatabase.collection("gory").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<IconModel> mCategory = new ArrayList<>();
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        idCategory = document.getId();
                        IconModel icon = document.toObject(IconModel.class);
                        mCategory.add(icon);
                    }
                    mCate.clear();
                    mCate.addAll(mCategory);
                    adapterCategory.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Loi", Toast.LENGTH_SHORT).show();
                    //
                }
            }
        });
    }
}