package com.example.order_food.model;

public class DetailBillModel {
    String addresscus, addressstore, iddetailfood, namecus, namestore, phonecus, phonestore, status;

    public DetailBillModel() {
    }

    public DetailBillModel(String addresscus, String addressstore, String iddetailfood, String namecus, String namestore, String phonecus, String phonestore, String status) {
        this.addresscus = addresscus;
        this.addressstore = addressstore;
        this.iddetailfood = iddetailfood;
        this.namecus = namecus;
        this.namestore = namestore;
        this.phonecus = phonecus;
        this.phonestore = phonestore;
        this.status = status;
    }

    public String getAddresscus() {
        return addresscus;
    }

    public void setAddresscus(String addresscus) {
        this.addresscus = addresscus;
    }

    public String getAddressstore() {
        return addressstore;
    }

    public void setAddressstore(String addressstore) {
        this.addressstore = addressstore;
    }

    public String getIddetailfood() {
        return iddetailfood;
    }

    public void setIddetailfood(String iddetailfood) {
        this.iddetailfood = iddetailfood;
    }

    public String getNamecus() {
        return namecus;
    }

    public void setNamecus(String namecus) {
        this.namecus = namecus;
    }

    public String getNamestore() {
        return namestore;
    }

    public void setNamestore(String namestore) {
        this.namestore = namestore;
    }

    public String getPhonecus() {
        return phonecus;
    }

    public void setPhonecus(String phonecus) {
        this.phonecus = phonecus;
    }

    public String getPhonestore() {
        return phonestore;
    }

    public void setPhonestore(String phonestore) {
        this.phonestore = phonestore;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
