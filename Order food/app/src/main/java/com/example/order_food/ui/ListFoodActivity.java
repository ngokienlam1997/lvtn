package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.order_food.Adapter.FoodAdapter;
import com.example.order_food.Adapter.ListFoodAdapter;
import com.example.order_food.R;
import com.example.order_food.model.FoodModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListFoodActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView view;
    FirebaseFirestore mDatabase;
    String nameCate;
    ProgressDialog progressDialog;
    ListFoodAdapter adapter;
    List<FoodModel> mFoods = new ArrayList<>();
    String idFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_food);

        toolbar = findViewById(R.id.tb_listfood);
        view = findViewById(R.id.view_listfood);

        mDatabase = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Vui long cho ...");
        progressDialog.setCanceledOnTouchOutside(false);

        Intent iin = getIntent();
        Bundle b = iin.getExtras();
        if (b != null) {
            nameCate = (String) b.get("KEY_CATE");
        }

        toolbar.setPadding(0, getStatusHeight(), 0, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Food");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        adapter = new ListFoodAdapter(this, mFoods);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        view.setLayoutManager(mLayoutManager);
        view.setItemAnimator(new DefaultItemAnimator());
        view.setAdapter(adapter);
        adapter.setOnItemClickListener((itemView, position)
                -> {
            Intent intent = new Intent(ListFoodActivity.this, AddOderActivity.class);
            intent.putExtra("KEY_NAME", mFoods.get(position).getIdstore());
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataListStore();
    }

    public void getDataListStore(){
        progressDialog.show();
        mDatabase.collection("food").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<FoodModel> mFood = new ArrayList<>();
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        idFood = document.getId();
                        FoodModel food = document.toObject(FoodModel.class);
                        mFood.add(food);
                    }
                    mFoods.clear();
                    for (int i = 0; i < mFood.size(); i++){
                        if (mFood.get(i).getCategory().equals(nameCate)){
                            mFoods.add(mFood.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ListFoodActivity.this, "Loi", Toast.LENGTH_SHORT).show();
                    //
                }
            }
        });
    }

    ////Transparent
    private int getStatusHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }
}