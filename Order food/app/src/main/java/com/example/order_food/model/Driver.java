package com.example.order_food.model;

public class Driver {
    private String key;

    public Driver() {
    }

    public Driver(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
