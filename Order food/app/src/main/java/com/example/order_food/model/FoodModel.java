package com.example.order_food.model;

public class FoodModel {
    String name, price, image, idstore, Category;

    public FoodModel() {
    }

    public FoodModel(String name, String price, String image, String idstore, String Category) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.idstore = idstore;
        this.Category = Category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdstore() {
        return idstore;
    }

    public void setIdstore(String idstore) {
        this.idstore = idstore;
    }

    public String getCategory() {
        return Category;
    }

    public void setCate(String Category) {
        this.Category = Category;
    }
}
