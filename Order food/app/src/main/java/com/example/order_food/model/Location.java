package com.example.order_food.model;

public class Location {

    public Double Latitude;
    public Double Longtitudel;

    public Location(Double latitude, Double longtitudel) {
        Latitude = latitude;
        Longtitudel = longtitudel;
    }

    public Location() {
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongtitudel() {
        return Longtitudel;
    }

    public void setLongtitudel(Double longtitudel) {
        Longtitudel = longtitudel;
    }
}
