package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.order_food.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegistrationActivity extends AppCompatActivity {

    EditText edt_firstName, edt_lastName, edt_phone, edt_email, edt_pass, edt_comPass;
    Button btn_signUp;

    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        edt_firstName = findViewById(R.id.edt_firstname);
        edt_lastName = findViewById(R.id.edt_lastname);
        edt_phone = findViewById(R.id.edt_phone);
        edt_email = findViewById(R.id.edt_resemail);
        edt_pass = findViewById(R.id.edt_respassword);
        edt_comPass = findViewById(R.id.edt_rescomfirmpassword);
        btn_signUp = findViewById(R.id.btn_ressignup);

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstName = edt_firstName.getText().toString();
                String lastName = edt_lastName.getText().toString();
                String phone = edt_phone.getText().toString();
                String email = edt_email.getText().toString();
                String pass = edt_pass.getText().toString();
                String comPass = edt_comPass.getText().toString();

                if(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(comPass)) {
                    Toast.makeText(RegistrationActivity.this, "Không được trống", Toast.LENGTH_SHORT).show();
                }else if (pass.length() < 6) {
                    Toast.makeText(RegistrationActivity.this, "Password không được bé hơn 6 ký tự", Toast.LENGTH_SHORT).show();
                }else {
                    registration(firstName, lastName, phone, email, pass);
                }

            }
        });
    }

    private void registration(final String firstName, final String lastName, final String phone, final String email, final String pass) {
        firebaseAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    String userId = firebaseUser.getUid();

                    reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("id", userId);
                    hashMap.put("firstname", firstName.toLowerCase());
                    hashMap.put("lastname", lastName);
                    hashMap.put("phone", phone);
                    hashMap.put("role", "2");
                    hashMap.put("address", "");

                    hashMap.put("imageurl","https://firebasestorage.googleapis.com/v0/b/applvtn.appspot.com/o/user.png?alt=media&token=dd6172f5-b0e9-41c8-9611-96a48fc30205");

                    reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                //progressDialog.dismiss();
                                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    });
                }else {
                    Toast.makeText(RegistrationActivity.this,"Tên đăng nhập hoặc mật khẩu không phù hợp",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}