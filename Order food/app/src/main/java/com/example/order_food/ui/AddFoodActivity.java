package com.example.order_food.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.order_food.R;
import com.example.order_food.model.FoodModel;
import com.example.order_food.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

public class AddFoodActivity extends AppCompatActivity {

    ////XML
    TextView edtAddress;
    EditText edtName, edtPrice;
    ImageView imgStore;
    Button button;
    Toolbar toolbar;

    ////Firebase
    FirebaseUser firebaseUser;
    String profileID;
    User user = new User();
    FoodModel foodModel = new FoodModel();
    Bitmap photo;
    ////image
    private static final int PICK_IMAGE = 100;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);

        ////Mapping
        edtName = findViewById(R.id.edt_foodname);
        edtPrice = findViewById(R.id.edt_foodprice);
        imgStore = findViewById(R.id.img_store);
        toolbar = findViewById(R.id.tb_store);
        button = findViewById(R.id.btn_storesave);

        ////Firebase
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences prefs = this.getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        profileID = prefs.getString("profileid", "none");

        imgStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                String price = edtPrice.getText().toString();
                String address = edtAddress.getText().toString();
                String image = BitMapToString(photo);

                Random random = new Random();
                int rd = random.nextInt(9999);
                String id = String.valueOf(rd);



                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(price)) {
                    Toast.makeText(AddFoodActivity.this, "Không được trống", Toast.LENGTH_SHORT).show();
                } else {
                    addFood(id, price, name, image, address);
                }
            }
        });

        ////Tool bar
        toolbar.setTitle("Store");
        toolbar.setPadding(0, getStatusHeight(), 0, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

    }

    ////add Gallery image
    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            if (data != null && Objects.requireNonNull(data.getExtras()).get("data") != null) {
                photo = (Bitmap) data.getExtras().get("data");
                //imgEmployee.setImageBitmap(photo);
                BitMapToString(photo);
            }
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream ByteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, ByteStream);
        byte[] b = ByteStream.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    ////add db and create when table db is null
    private void addFood(final String foodID, final String foodPrice, final String foodName, final String foodImage, final String foodAddress) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Foods").child(foodID);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("foodID", foodID);
        hashMap.put("foodPrice", foodPrice);
        hashMap.put("foodName", foodName);
        hashMap.put("foodImage", foodImage);
        hashMap.put("foodAddress", foodAddress);

        reference.push().setValue(hashMap);
        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    //progressDialog.dismiss();
                    Intent intent = new Intent(AddFoodActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(AddFoodActivity.this,"Bạn đã thêm thành công món ăn",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    ////Transparent
    private int getStatusHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier("status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }
}