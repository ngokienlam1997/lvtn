package com.example.order_food.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.order_food.R;
import com.example.order_food.fragment.OrderFragment;
import com.example.order_food.model.BillModel;
import com.example.order_food.model.FoodDetailModel;
import com.example.order_food.model.FoodModel;
import com.example.order_food.ui.AddStoreActivity;
import com.example.order_food.ui.StoreActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddOrderAdapter extends RecyclerView.Adapter<AddOrderAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<FoodModel> mFood;
    private FirebaseFirestore mDatabase;

    private static OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public AddOrderAdapter(Context context, List<FoodModel> mFood) {
        this.layoutInflater = layoutInflater.from(context);
        this.context = context;
        this.mFood = mFood;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.add_order_adapter, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        FoodModel mFoods = mFood.get(position);
        holder.name.setText(mFoods.getName());
        holder.price.setText(mFoods.getPrice());
        holder.address.setText(mFoods.getIdstore());
        if (mFoods.getImage() != null) {
            byte[] decodedString = Base64.decode(mFoods.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.imageView.setImageBitmap(decodedByte);
        } else {
            holder.imageView.setImageBitmap(null);
        }
    }

    @Override
    public int getItemCount() {
        return mFood.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, price, address;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_store_name_adapter);
            price = itemView.findViewById(R.id.txt_store_address_adapter);
            address = itemView.findViewById(R.id.txt_store_phone_adapter);
            imageView = itemView.findViewById(R.id.img_store_adapter);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(itemView, getLayoutPosition());
                }
            });
            itemView.findViewById(R.id.btn_addorder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();
                    Toast.makeText(itemView.getContext(), Integer.toString(position), Toast.LENGTH_SHORT).show();

                    if (getLayoutPosition() == position){
                        Intent intent = new Intent(v.getContext(), OrderFragment.class);
                        intent.putExtra("name", name.getText().toString());
                        intent.putExtra("price", price.getText().toString());
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}
