package com.example.order_food.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.order_food.R;
import com.example.order_food.model.StoreModel;

import java.util.List;

public class ListStoreAdapter extends RecyclerView.Adapter<ListStoreAdapter.MyViewHolder> {

    List<StoreModel> mStore;
    Context context;
    private LayoutInflater layoutInflater;
    private static OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ListStoreAdapter(List<StoreModel> mStore, Context context) {
        this.mStore = mStore;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.food_home_adapter, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        StoreModel mStores = mStore.get(position);
        holder.name.setText(mStores.getName());
        holder.address.setText(mStores.getAddress());
        if (mStores.getImage() != null) {
            byte[] decodedString = Base64.decode(mStores.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.imageView.setImageBitmap(decodedByte);
        } else {
            holder.imageView.setImageBitmap(null);
        }
    }

    @Override
    public int getItemCount() {
        return mStore.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, address;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_namestore);
            address = itemView.findViewById(R.id.txt_addressstore);
            imageView = itemView.findViewById(R.id.img_imagestore);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
