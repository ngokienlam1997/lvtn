package com.example.order_food.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.order_food.R;
import com.example.order_food.action.LatLngInterpolator;
import com.example.order_food.action.MarkerAnimation;
import com.example.order_food.ui.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener {

    private GoogleMap mMap;
    private SupportMapFragment supportMapFragment;
    private FusedLocationProviderClient client;
    private Context context;

    private static final String LOCATION = "Location";
    private static final String LATITUDE = "latitude";
    private static final String LONGTITUDE = "longitude";
    private HashMap<String, Marker> mMarkers = new HashMap<>();
    private ArrayList<String> drivers = new ArrayList<>();
    private LatLng location, oldLocation;
    private FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private CollectionReference collections = firestore.collection(LOCATION);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        context = getActivity();
        if (isNetworkConnected() && isGooglePlayServicesAvailable() && canGetLocation()) {
            SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
            requestPermission();

        } else {
            showDialog();
        }
        return view;
    }

//    private void getLocation() {
//        @SuppressLint("MissingPermission") Task<Location> task = client.getLastLocation();
//        task.addOnSuccessListener(new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(final Location location) {
//                if (location != null) {
//                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
//
//                        @Override
//                        public void onMapReady(GoogleMap googleMap) {
//                            mMap = googleMap;
//                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                            mMap.addMarker(new MarkerOptions().position(latLng).title("Vị trí của bạn"));
//                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                        }
//                    });
//                }
//            }
//        });
//    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(20.0f);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        addPolyline();
        //-------------//
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(10.803111, 106.617483))
                .title("AEON Small Tân Phú"));
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(10.743139, 106.612081))
                .title("AEON Small Bình Tân"));
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(10.932598, 106.711996))
                .title("AEON Small Bình Dương"));
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(21.027182, 105.898902))
                .title("AEON Small Long Biên"));
        //--------------//
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.762622, 106.660172), 16));
        getLocation();
        //showMarkerTest();
    }

    private void addPolyline() {
        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(10.786150, 106.665983),
                        new LatLng(10.786869, 106.664590),
                        new LatLng(10.785815, 106.663624),
                        new LatLng(10.785056, 106.662927),
                        new LatLng(10.784076, 106.662036),
                        new LatLng(10.782507, 106.660502),
                        new LatLng(10.780621, 106.658717),
                        new LatLng(10.779361, 106.657549),
                        new LatLng(10.777997, 106.656180),
                        new LatLng(10.778520, 106.655873),
                        new LatLng(10.780080, 106.655422),
                        new LatLng(10.781746, 106.654980),
                        new LatLng(10.782934, 106.654610),
                        new LatLng(10.784538, 106.654159),
                        new LatLng(10.787507, 106.653284),
                        new LatLng(10.789138, 106.652806),
                        new LatLng(10.790210, 106.652427),
                        new LatLng(10.791663, 106.652770),
                        new LatLng(10.792886, 106.653365),
                        new LatLng(10.794100, 106.654448),
                        new LatLng(10.795395, 106.655697),
                        new LatLng(10.796529, 106.654646),
                        new LatLng(10.796073, 106.654182),
                        new LatLng(10.795666, 106.653756))
                .width(12)
                .color(Color.GREEN));
    }

    public void showMarker(DocumentSnapshot document) {
        try {
            //String key = "123456";
            String key = document.getId();
            HashMap<String, Object> value = (HashMap<String, Object>) document.getData();
            assert value != null;
            double lat = Double.parseDouble(Objects.requireNonNull(value.get(LATITUDE)).toString());
            double lng = Double.parseDouble(Objects.requireNonNull(value.get(LONGTITUDE)).toString());
            if (oldLocation == null) {
                location = new LatLng(lat, lng);
                oldLocation = new LatLng(lat, lng);
            } else {
                oldLocation = location;
                location = new LatLng(lat, lng);
            }

            if (!mMarkers.containsKey(key))
                mMarkers.put(key, mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_logo))
                        .position(location)
                        .title(key)));
            else {
                //mMarkers.get(key).setPosition(latLng);
                MarkerAnimation.animateMarkerToGB(mMarkers.get(key),location, new LatLngInterpolator.Spherical());
                float bearing = getBearingFrom(oldLocation, location);
                mMarkers.get(key).setRotation(bearing);
                animateCamera(location);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void getLocation() {
        collections.document("driver001").addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                showMarker(documentSnapshot);
                Log.e("TAG", documentSnapshot.getId() + "=>" + documentSnapshot.getData());
            }
        });
    }
    private void animateCamera(@NonNull LatLng location) {
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(location)));
    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }

    private float getBearingFrom(LatLng first, LatLng end) {
        return (float) SphericalUtil.computeHeading(first, end);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(MainActivity.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(getContext());
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(getContext(), "Please Install google play services to use this application", Toast.LENGTH_LONG).show();
        }
        return false;
    }
    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Ahii")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (!isNetworkConnected()) {
                            startActivityForResult(new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS), 1);
                        } else if (!canGetLocation()) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean canGetLocation() {
        LocationManager manager = (LocationManager) getContext().getSystemService(MainActivity.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("TAG", "Permission has already been granted");
            }
        }
    }

    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        super.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

}