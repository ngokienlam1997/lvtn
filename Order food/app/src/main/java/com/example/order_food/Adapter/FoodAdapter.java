package com.example.order_food.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.order_food.R;
import com.example.order_food.model.FoodModel;
import com.example.order_food.model.StoreModel;

import java.util.List;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<FoodModel> mFood;

    private static OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public FoodAdapter( Context context, List<FoodModel> models) {
        this.context = context;
        this.mFood = models;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.store_adapter, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        FoodModel mFoods = mFood.get(position);
        holder.name.setText(mFoods.getName());
        holder.price.setText(mFoods.getPrice());
        holder.address.setText(mFoods.getIdstore());
        if (mFoods.getImage() != null) {
            byte[] decodedString = Base64.decode(mFoods.getImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.imageView.setImageBitmap(decodedByte);
        } else {
            holder.imageView.setImageBitmap(null);
        }
    }

    @Override
    public int getItemCount() {
        return mFood.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, price, address;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_store_name_adapter);
            price = itemView.findViewById(R.id.txt_store_address_adapter);
            address = itemView.findViewById(R.id.txt_store_phone_adapter);
            imageView = itemView.findViewById(R.id.img_store_adapter);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}
