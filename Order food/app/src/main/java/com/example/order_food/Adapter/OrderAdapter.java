package com.example.order_food.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.order_food.R;
import com.example.order_food.model.OrderModel;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.Viewholder> {

    ArrayList<OrderModel> orderModels;
    Context context;

    public OrderAdapter(ArrayList<OrderModel> orderModels, Context context) {
        this.orderModels = orderModels;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_adapter, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.img_order.setImageResource(Integer.parseInt(orderModels.get(position).getImageURL()));
        holder.txt_name.setText(orderModels.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView img_order;
        TextView txt_name, txt_price;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            img_order = itemView.findViewById(R.id.img_order);
            txt_name = itemView.findViewById(R.id.txt_ordername);
            txt_price = itemView.findViewById(R.id.txt_orderprice);
        }
    }
}
