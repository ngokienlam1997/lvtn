package com.example.order_food.model;

public class FoodDetailModel {
    private String name;
    private String price;
    private String picture;
    private String discription;

    public FoodDetailModel() {
    }

    public FoodDetailModel(String name, String price, String picture, String discription) {
        this.name = name;
        this.price = price;
        this.picture = picture;
        this.discription = discription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }
}
